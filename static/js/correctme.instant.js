'use strict';

class AddressInstantSuggest {

    static instance;

    static create(options) {
        if (AddressInstantSuggest.instance != null) {
            return AddressInstantSuggest.instance;
        } else {
            AddressInstantSuggest.instance = new AddressInstantSuggest(options);
            return AddressInstantSuggest.instance;

        }
    }

    constructor(options) {
        this._options = options;
    }

    get options() {
        return this._options;
    }

    set options(value) {
        this._options = value;
    }

    _options = {};

    static cache = {};

    static $j = jQuery.noConflict();

    static getJQuery() {
        return AddressInstantSuggest.$j;
    }

    static setJQuery(jq) {
        return AddressInstantSuggest.$j = jq;
    }

    static getUpdated(term, suggestions) {
        if (suggestions.length > 0) {
            AddressInstantSuggest.cache[term] = suggestions;
            return suggestions;
        }
    }

    suggest(term) {
        if (typeof this._options !== "undefined" && this._options != null) {
            if (term in AddressInstantSuggest.cache) {
                return AddressInstantSuggest.cache[term];
            }

            var suggestions = [];
            console.log(this._options.host + "/api/v1/suggestAddress?etoken=testEtoken&address=" + term);
            $j.ajax({
                url: this._options.host + "/api/v1/suggestAddress?etoken=" + this._options.token + "&address=" + term,
                async: false
            }).done(function (data) {
                if (typeof data.response !== "undefined" && data.response != null) {
                    data.response.values.forEach(function (a) {
                        console.log(a);
                        suggestions.push({label: a.suggestion});
                    });
                    AddressInstantSuggest.getUpdated(term, suggestions);
                }
            }).fail(function () {
                console.log("Request failed for : " + term);
            });
            return suggestions;
        }
    }
}

class AddressInstantCorrect {

    static instance;

    static create(options) {
        if (AddressInstantCorrect.instance != null) {
            return AddressInstantCorrect.instance;
        } else {
            AddressInstantCorrect.instance = new AddressInstantCorrect(options);
            return AddressInstantCorrect.instance;
        }
    }

    constructor(options) {
        this._options = options;
    }

    get options() {
        return this._options;
    }

    set options(value) {
        this._options = value;
    }

    _options = {};

    static cache = {};

    static $j = jQuery.noConflict();

    static getJQuery() {
        return AddressInstantCorrect.$j;
    }

    static setJQuery(jq) {
        return AddressInstantCorrect.$j = jq;
    }

    correct(term) {
        if (typeof this._options !== "undefined" && this._options != null) {

            let address_line = $j('#' + AddressInstantCorrect.instance._options.address_line);
            let zip = $j('#' + AddressInstantCorrect.instance._options.zip);
            let district = $j('#' + AddressInstantCorrect.instance._options.district);
            let area = $j('#' + AddressInstantCorrect.instance._options.area);
            let city = $j('#' + AddressInstantCorrect.instance._options.city);
            let street = $j('#' + AddressInstantCorrect.instance._options.street);
            let num = $j('#' + AddressInstantCorrect.instance._options.number);

            $j.get({
                url: this._options.host + "/api/v1/correctAddress?etoken=" + this._options.token + "&address=" + term,
                async: false
            }).then(function (data) {
                if (typeof data.response !== "undefined" && data.response != null) {
                    address_line.attr('class', AddressInstantCorrect.instance._options.verified);
                    address_line.val(data.response.postCode + ", "
                        + data.response.area + " обл., "
                        + data.response.localityType
                        + data.response.locality
                        + ", " + data.response.streetType
                        + data.response.street
                        + ", буд." + data.response.streetNumber);
                    zip.val(data.response.postCode || "");
                    district.val(data.response.area || "");
                    area.val(data.response.region || "");
                    city.val(data.response.localityType + data.response.locality || "");
                    street.val(data.response.streetType + data.response.street || "");
                    num.val(data.response.streetNumber || "");
                } else {
                    address_line.attr('class', AddressInstantCorrect.instance._options.error);
                    address_line.val("");
                    zip.val("");
                    district.val("");
                    area.val("");
                    city.val("");
                    street.val("");
                    num.val("");
                }
            }).fail(function () {
                address_line.attr('class', AddressInstantCorrect.instance._options.error);
                address_line.val("");
                zip.val("");
                district.val("");
                area.val("");
                city.val("");
                street.val("");
                num.val("");
            });
        }
    }
}


class FullnameInstantSuggest {

    static instance;

    static create(options) {
        if (FullnameInstantSuggest.instance != null) {
            return FullnameInstantSuggest.instance;
        } else {
            FullnameInstantSuggest.instance = new FullnameInstantSuggest(options);
            return FullnameInstantSuggest.instance;

        }
    }

    constructor(options) {
        this._options = options;
    }

    get options() {
        return this._options;
    }

    set options(value) {
        this._options = value;
    }

    _options = {};

    static cache = {};
    static suggestions_cache = [];
    static suggestions_plain = [];

    static $j = jQuery.noConflict();

    static getJQuery() {
        return FullnameInstantSuggest.$j;
    }

    fill(term) {
        if (typeof this._options !== "undefined" && this._options != null) {
            console.log("fill:" + term);
            if ($j.inArray(term, FullnameInstantSuggest.suggestions_plain) > -1) {
                var fiog = FullnameInstantSuggest.suggestions_cache[$j.inArray(term, FullnameInstantSuggest.suggestions_plain)];
                $j('#' + FullnameInstantSuggest.instance._options.fullname).val(fiog.suggestion);
                $j('#' + FullnameInstantSuggest.instance._options.name).val(fiog.name);
                $j('#' + FullnameInstantSuggest.instance._options.thirdname).val(fiog.thirdName);
                $j('#' + FullnameInstantSuggest.instance._options.surname).val(fiog.surName);
                $j('#' + FullnameInstantSuggest.instance._options.gender).val(fiog.gender);
            }
        }
    }

    suggest(term) {
        if (typeof this._options !== "undefined" && this._options != null) {
            if (term in FullnameInstantSuggest.cache) {
                return FullnameInstantSuggest.cache[term];
            }
            console.log(this._options.host + "/api/v1/suggestFullName?etoken=testEtoken&fuzzy=true&name=" + term);
            var suggestions = [];
            $j.ajax({
                url: this._options.host + "/api/v1/suggestFullName?etoken=" + this._options.token + "&name=" + term,//.replace(/ /g,"%20")
                async: false
            }).done(function (data) {
                if (typeof data.response !== "undefined" && data.response != null) {
                    FullnameInstantSuggest.suggestions_cache = [];
                    FullnameInstantSuggest.suggestions_plain = [];
                    data.response.values.forEach(function (a) {
                        console.log(a);
                        suggestions.push({label: a.suggestion});
                        FullnameInstantSuggest.suggestions_cache.push(a);
                        FullnameInstantSuggest.suggestions_plain.push(a.suggestion);
                    });

                    FullnameInstantSuggest.cache[term] = suggestions;
                }
            }).fail(function () {
                console.log("Request failed for : " + term);
            });
            return suggestions;
        }
    }
}

var $j = AddressInstantSuggest.getJQuery();
$j(document).ready(function () {
    $j.fn.addressLineInstantSuggest = function (options) {
        const address = AddressInstantSuggest.create(options);
        $j('#' + options.input_id).autocomplete({
            minLength: 1,
            source: function (request, response) {
                var r = address.suggest(request.term);
                console.log(r);
                response(r);
            }
        });
    };
});

// $j = AddressInstantCorrect.getJQuery();
$j(document).ready(function () {
    $j.fn.addressLineInstantCorrect = function (options) {

        const address = AddressInstantCorrect.create(options);

        $j.fn.enterKey = function (fnc) {
            return this.each(function () {
                $j(this).keypress(function (ev) {
                    var keycode = (ev.keyCode ? ev.keyCode : ev.which);
                    if (keycode == '13') {
                        fnc.call(this, ev);
                    }
                })
            })
        };

        $j('#' + options.input_id).on('autocompletechange change', function () {
            address.correct($j(this).val());
        }).change();

        $j('#' + options.button_id).click(function (event) {
            event.preventDefault();
            $j(".ui-menu-item").hide();
            address.correct($j('#' + id).val());
        });

        $j('#' + options.input_id).enterKey(function () {
            $j(".ui-menu-item").hide();
            address.correct($j(this).val());
        });

    };
});


// $j = FullnameInstantSuggest.getJQuery();
$j(document).ready(function () {
    $j.fn.fullnameLineInstantSuggest = function (options) {
        const fullname = FullnameInstantSuggest.create(options);

        $j(function () {
            $j('#' + options.input_id).autocomplete({
                minLength: 2,
                source: function (request, response) {
                    var r = fullname.suggest(request.term);
                    console.log(r);
                    response(r);
                }
            });
        });

        $j.fn.enterKey = function (fnc) {
            return this.each(function () {
                $j(this).keypress(function (ev) {
                    var keycode = (ev.keyCode ? ev.keyCode : ev.which);
                    if (keycode == '13') {
                        fnc.call(this, ev);
                    }
                })
            })
        };

        $j('#' + options.input_id).enterKey(function () {
            fullname.fill(this.value);
        });

        $j('#' + options.input_id).on('autocompletechange change', function () {
            fullname.fill(this.value);
        }).change();
    }
});

